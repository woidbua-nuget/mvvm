﻿using System;
using System.Threading.Tasks;

namespace MVVM.Commands
{
    public sealed class AsyncDelegateCommand : AsyncDelegateCommandBase
    {
        private readonly Func<Task> _executeMethod;
        private readonly Func<bool> _canExecuteMethod;


        public AsyncDelegateCommand(Func<Task> executeMethod)
            : this(executeMethod, () => true)
        {
        }


        public AsyncDelegateCommand(Func<Task> executeMethod, Func<bool> canExecuteMethod)
        {
            if (executeMethod == null || canExecuteMethod == null)
                throw new ArgumentNullException(
                    nameof(executeMethod),
                    Constants.Commands.ExceptionMessages.DelegateCannotBeNull
                );

            _executeMethod = executeMethod;
            _canExecuteMethod = canExecuteMethod;
        }


        public bool CanExecute()
        {
            return _canExecuteMethod();
        }


        protected override bool CanExecute(object parameter)
        {
            return CanExecute();
        }


        public Task Execute()
        {
            return _executeMethod();
        }


        protected override Task Execute(object parameter)
        {
            return Execute();
        }
    }


    public sealed class AsyncDelegateCommand<T> : AsyncDelegateCommandBase
    {
        private readonly Func<T, Task> _executeMethod;
        private readonly Func<T, bool> _canExecuteMethod;


        public AsyncDelegateCommand(Func<T, Task> executeMethod)
            : this(executeMethod, o => true)
        {
        }


        public AsyncDelegateCommand(Func<T, Task> executeMethod, Func<T, bool> canExecuteMethod)
        {
            if (executeMethod == null || canExecuteMethod == null)
                throw new ArgumentNullException(
                    nameof(executeMethod),
                    Constants.Commands.ExceptionMessages.DelegateCannotBeNull
                );

            _executeMethod = executeMethod;
            _canExecuteMethod = canExecuteMethod;
        }


        public bool CanExecute(T parameter)
        {
            return _canExecuteMethod(parameter);
        }


        protected override bool CanExecute(object parameter)
        {
            return CanExecute((T) parameter);
        }


        public Task Execute(T parameter)
        {
            return _executeMethod(parameter);
        }


        protected override Task Execute(object parameter)
        {
            return Execute((T) parameter);
        }
    }
}

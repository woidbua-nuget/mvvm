﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MVVM.Commands
{
    public abstract class AsyncDelegateCommandBase : ICommand
    {
        public event EventHandler? CanExecuteChanged;


        bool ICommand.CanExecute(object parameter)
        {
            return CanExecute(parameter);
        }


        async void ICommand.Execute(object parameter)
        {
            await Execute(parameter);
        }


        protected abstract bool CanExecute(object parameter);

        protected abstract Task Execute(object parameter);


        public void RaiseCanExecuteChanged()
        {
            OnCanExecuteChanged();
        }


        protected virtual void OnCanExecuteChanged()
        {
            CanExecuteChanged?.Invoke(this, EventArgs.Empty);
        }
    }
}

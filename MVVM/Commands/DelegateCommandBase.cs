﻿using System;
using System.Windows.Input;

namespace MVVM.Commands
{
    public abstract class DelegateCommandBase : ICommand
    {
        public event EventHandler? CanExecuteChanged;


        bool ICommand.CanExecute(object parameter)
        {
            return CanExecute(parameter);
        }


        void ICommand.Execute(object parameter)
        {
            Execute(parameter);
        }


        protected abstract bool CanExecute(object parameter);

        protected abstract void Execute(object parameter);


        public void RaiseCanExecuteChanged()
        {
            OnCanExecuteChanged();
        }


        protected virtual void OnCanExecuteChanged()
        {
            CanExecuteChanged?.Invoke(this, EventArgs.Empty);
        }
    }
}

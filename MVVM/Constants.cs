﻿namespace MVVM
{
    internal static class Constants
    {
        public static class Commands
        {
            public static class ExceptionMessages
            {
                public const string DelegateCannotBeNull =
                    "The execution method for the delegate command should not be null.";
            }
        }
    }
}
